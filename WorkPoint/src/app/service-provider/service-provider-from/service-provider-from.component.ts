import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceProviderService } from '../service-provider.service';
import { state } from '../../dummyData/state';
import { MessageService } from 'src/app/core/Services/message.service';
@Component({
  selector: 'ws-service-provider-from',
  templateUrl: './service-provider-from.component.html',
  styleUrls: ['./service-provider-from.component.less'],
})
export class ServiceProviderFromComponent implements OnInit {
  skill = [];
  aadharImage: any;
  userPic: any;
  subCat: any;
  Statename = state;
  errorMessage = {
    mobileNumberError: '',
    aadharNumberError: '',
    passwordMatch: '',
  };
  mobileNumberRegex = new RegExp('^((\\+91-?)|0)?[0-9]{10}$');
  aadharNumberRex = new RegExp('^((\\+91-?)|0)?[0-9]{12}$');
  lowercaseregex = new RegExp('(?=.*[a-z])');
  uppercaseregx = new RegExp('(?=.*[A-Z])');
  digitregex = new RegExp('(?=.*[0-9])');
  specialcharregex = new RegExp('(?=.*[!,@,#,$,%,&])');

  constructor(
    private serviceProvider: ServiceProviderService,
    private message: MessageService
  ) {}

  ngOnInit(): void {
    this.getSubcatgory();
  }
  checkFormValidation(value): any {
    if (!this.mobileNumberRegex.test(value.mobileNumber)) {
      this.errorMessage.mobileNumberError =
        'Enter valid mobile number, do not add +91';
      return false;
    } else if (this.aadharNumberRex.test(value.aadharNumber)) {
      this.errorMessage.aadharNumberError = 'Enter valid aadhar number';
      return false;
    } else if (!this.lowercaseregex.test(value.password)) {
      this.errorMessage.passwordMatch =
        'Password must contain at least one lower case letter';
      return false;
    } else if (!this.uppercaseregx.test(value.password)) {
      this.errorMessage.passwordMatch =
        'Password must contain at least one upper case letter';
      return false;
    } else if (!this.digitregex.test(value.password)) {
      this.errorMessage.passwordMatch =
        'Password must contain at least one digit letter';
      return false;
    } else if (!this.specialcharregex.test(value.password)) {
      this.errorMessage.passwordMatch =
        'Password must contain at least one special character';
      return false;
    } else if (value.password.length < 8) {
      this.errorMessage.passwordMatch =
        'Password must contain at least content 8 character';
      return false;
    } else if (value.password !== value.cpassword) {
      this.errorMessage.passwordMatch = 'Password Not Matched';
      return false;
    } else {
      return true;
    }
  }
  submitForm(value): void {
    if (this.checkFormValidation(value)) {
      // tslint:disable-next-line: prefer-const
      let formData = new FormData();
      // tslint:disable-next-line: prefer-const
      // tslint:disable-next-line: forin
      for (let key in value) {
        formData.append(key, value[key]);
      }
      formData.append('aadhar', this.aadharImage);
      formData.append('profilephoto', this.userPic);
      this.serviceProvider.registerServiceProvider(formData).subscribe(
        (res) => this.message.showMessage('success', res),
        (err) => this.message.showMessage('error', err.error)
      );
      console.log(value);
    }
  }
  resetForm(form: NgForm): any {
    form.reset();
  }
  getAadharImage(event): any {
    this.aadharImage = event.target.files[0];
    console.log(this.aadharImage);
  }
  getUserPhoto(event): any {
    this.userPic = event.target.files[0];
    console.log(this.userPic);
  }
  getSubcatgory(): any {
    this.serviceProvider.getCatgory().subscribe((res) => {
      console.log(res);
      this.subCat = res;
    });
  }
  clearErrorMessage(): any {
    this.errorMessage = {
      mobileNumberError: '',
      aadharNumberError: '',
      passwordMatch: '',
    };
  }
}
