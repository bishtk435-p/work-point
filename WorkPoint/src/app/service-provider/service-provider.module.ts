import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceProviderRoutingModule } from './service-provider-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { ServiceProviderFromComponent } from './service-provider-from/service-provider-from.component';
import { AntDesignModule } from '../ant-design/ant-design.module';
import { FormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [ProfileComponent, ServiceProviderFromComponent],
  imports: [
    CommonModule,
    ServiceProviderRoutingModule,
    AntDesignModule,
    FormsModule,
    NgxSpinnerModule,
  ],
})
export class ServiceProviderModule {}
