import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { AdminApiService } from '../core/Services/admin-api.service';

@Injectable({
  providedIn: 'root',
})
export class ServiceProviderService {
  filterServiceProvider = new EventEmitter();
  constructor(
    private http: HttpClient,
    private adminService: AdminApiService
  ) {}

  registerServiceProvider(FormData: any): any {
    // for (var value of FormData.keys()) {
    //   console.log(value);
    // }
    // for (var value1 of FormData.values()) {
    //   console.log(value1);
    // }
    return this.http.post(
      `${environment.nodeServerUrl}service/addserviceProvider`,
      FormData
    );
  }

  getCatgory(): any {
    return this.adminService.getCategory();
  }

  getServices(paramId: any): Observable<any> {
    return this.http.get(
      `${environment.nodeServerUrl}service/selectServiceId`,
      { params: { serviceId: paramId } }
    );
  }

  filterServiceProvides(params: any): Observable<any> {
    return this.http.get(
      `${environment.nodeServerUrl}service/filterserviceprovider`,
      { params: { serviceId: params } }
    );
  }
  getAllServiceProvides(): Observable<any> {
    return this.http.get(
      `${environment.nodeServerUrl}service/getserviceprovider`
    );
  }

  getServicesRequest(params: any): Observable<any> {
    return this.http.get(`${environment.nodeServerUrl}users/getrequest`, {
      params: { userId: params },
    });
  }
  getPostedRequest(params: any): Observable<any> {
    return this.http.get(`${environment.nodeServerUrl}users/userrequest`, {
      params: { userId: params },
    });
  }

  approveRequest(params: any): Observable<any> {
    return this.http.post(`${environment.nodeServerUrl}users/acceptrequest`, {
      params,
    });
  }

  filterRequest(params: any): Observable<any> {
    return this.http.get(`${environment.nodeServerUrl}users/filterrequest`, {
      params: { userId: params.userid, status: params.status },
    });
  }
  filterProviderRequest(params: any): Observable<any> {
    return this.http.get(
      `${environment.nodeServerUrl}users/filterproviderrequest`,
      {
        params: { userId: params.userid, status: params.status },
      }
    );
  }
}
