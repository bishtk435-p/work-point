import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestedServicesComponent } from './requested-services.component';

describe('RequestedServicesComponent', () => {
  let component: RequestedServicesComponent;
  let fixture: ComponentFixture<RequestedServicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestedServicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestedServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
