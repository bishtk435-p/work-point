import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ApiService } from 'src/app/core/Services/api.service';

@Component({
  selector: 'ws-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.less']
})
export class PaymentComponent implements OnInit {
  PAYPAL_URL="https://www.sandbox.paypal.com/cgi-bin/webscr"
  PAYPAL_ID="sb-vj47lg1212144@business.example.com"
  reqId;
  userId = localStorage.getItem('userid')
  paymentDetail;
  providerId;
  loader = false
  constructor(
    private route : ActivatedRoute,
    private apiService: ApiService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.reqId = this.route.snapshot.queryParams.reqId;
    this.providerId = this.route.snapshot.queryParams.providerId
    this.spinner.show();
    this.apiService.getPaymentDetail(this.reqId).subscribe(
      res => {
        console.log(res)
        this.paymentDetail = res;
        this.spinner.hide();
      },
      err => {
        this.spinner.hide();
      }
    )
  }

}
