import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/core/Services/api.service';

@Component({
  selector: 'ws-cancle-payment',
  templateUrl: './cancle-payment.component.html',
  styleUrls: ['./cancle-payment.component.less'],
})
export class CanclePaymentComponent implements OnInit {
  transaction: String;
  requestId: String;
  paymentDate: Date;
  paidAmount: String;
  paidby: String;
  paidTo: String;
  paymentStatue: String;
  paymentDetail: any;
  username = localStorage.getItem('userName');
  constructor(private router: ActivatedRoute, private apiService: ApiService) {}

  ngOnInit(): void {
    // this.apiService.updateItem.subscribe((res) => (this.paymentDetail = res));
    this.router.queryParams.subscribe((res) => {
      console.log(res);
      (this.transaction = res.PayerID), (this.requestId = res.rowId);
      this.paidAmount = res.amt;
      this.paidby = res.userId;
      this.paidTo = res.paymentTo;
      const saveData = {
        transaction: this.transaction,
        requestId: this.requestId,
        paidAmount: this.paidAmount,
        paidby: this.paidby,
        paidTo: this.paidTo,
        paymentStatus: 'success',
      };
      this.apiService.savePaymentDetail(saveData).subscribe((res: any) => {
        // console.log(res);
        // (this.transaction = res.PayerID), (this.requestId = res.rowId);
        // this.paidAmount = res.amt;
        // this.paidby = res.userId;
        // this.paidTo = res.paymentTo;
        // let saveData = {
        //   transaction: this.transaction,
        //   requestId: this.requestId,
        //   paidAmount: this.paidAmount,
        //   paidby: this.paidby,
        //   paidTo: this.paidTo,
        //   paymentStatus: 'success',
        // };
        // this.apiService.savePaymentDetail(saveData).subscribe((res) => {
        //   console.log('response==', res);
          this.paymentDetail = res;
          // this.apiService.updateItem.emit(res);
        // });
      });
    });
  }
}

//         this.apiService.updateItem.emit(res);
//       });
//     });
//   }
// }
