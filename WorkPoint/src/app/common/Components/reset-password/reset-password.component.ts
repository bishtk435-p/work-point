import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { MessageService } from 'src/app/core/Services/message.service';

@Component({
  selector: 'ws-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.less'],
})
export class ResetPasswordComponent implements OnInit {
  loader: any;
  passwordtype = 'password';
  cpasswordtype = 'password';
  passwordMatch: any;
  lowercaseregex = new RegExp('(?=.*[a-z])');
  uppercaseregx = new RegExp('(?=.*[A-Z])');
  digitregex = new RegExp('(?=.*[0-9])');
  specialcharregex = new RegExp('(?=.*[!,@,#,$,%,&])');
  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}
  changetype(value): void {
    this.passwordtype = value;
  }
  changetypec(value): void {
    this.cpasswordtype = value;
  }

  clearErr(): void {
    this.passwordMatch = null;
  }

  checkFormData(value: any): boolean {
    if (!this.lowercaseregex.test(value.password)) {
      this.passwordMatch =
        'Password must contain at least one lower case letter';
      return false;
    } else if (!this.uppercaseregx.test(value.password)) {
      this.passwordMatch =
        'Password must contain at least one upper case letter';
      return false;
    } else if (!this.digitregex.test(value.password)) {
      this.passwordMatch = 'Password must contain at least one digit letter';
      return false;
    } else if (!this.specialcharregex.test(value.password)) {
      this.passwordMatch =
        'Password must contain at least one special character';
      return false;
    } else if (value.password.length < 8) {
      this.passwordMatch = 'Password must contain at least content 8 character';
      return false;
    } else if (value.password !== value.cpassword) {
      this.passwordMatch = 'Password Not Matched';
      return false;
    } else {
      return true;
    }
  }

  formSubmit(value): any {
    if (this.checkFormData(value)) {
      const formData = {
        ...value,
        email: this.route.snapshot.queryParams.email,
      };
      this.loader = true;
      this.auth.resetPassword(formData).subscribe(
        (res) => {
          this.loader = false;
          this.messageService.showMessage('success', res);
        },
        (err) => {
          this.loader = false;
          this.messageService.showMessage('error', err);
        }
      );
    }
  }
}
