import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { NzDrawerRef } from 'ng-zorro-antd/drawer';
import { MessageService } from 'src/app/core/Services/message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ws-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.less'],
})
export class DrawerComponent implements OnInit {
  currentUser = false;
  userRole: any;
  constructor(
    private authService: AuthService,
    private drawerRef: NzDrawerRef,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const userRole = localStorage.getItem('userRole');
    this.userRole = userRole;
    if (userRole === 'user') {
      this.currentUser = true;
    }
  }

  signout(): void {
    this.drawerRef.close();
    localStorage.removeItem('token');
    localStorage.removeItem('userName');
    localStorage.removeItem('userid');
    localStorage.removeItem('userRole');
    this.authService.isuserLoggedIn.emit('logout');
    this.messageService.showMessage('success', 'Logged out Successfully');
    this.router.navigate(['/']);
  }

  onclick(): void {
    this.drawerRef.close();
  }
}
