import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { MessageService } from 'src/app/core/Services/message.service';

@Component({
  selector: 'ws-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.less']
})
export class ForgetPasswordComponent implements OnInit {
  loader: any;
  emailValue: any;
  constructor(
    private auth: AuthService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
  }
  formSubmit(value): any{
    console.log(value);
    this.loader = true;
    this.auth.forgetPassword(value).subscribe(
      res => {
        this.emailValue = '';
        this.messageService.showMessage('success', res.msg),
        this.loader = false;
      },
      err => {
        this.messageService.showMessage('error', err.error);
        this.loader = false;
      }
    );
  }
}
