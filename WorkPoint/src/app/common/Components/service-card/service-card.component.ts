import {
  Component,
  OnInit,
  Input,
  ViewEncapsulation,
  OnChanges,
} from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'ws-service-card',
  templateUrl: './service-card.component.html',
  styleUrls: ['./service-card.component.less'],
})
export class ServiceCardComponent implements OnInit {
  @Input() cardDetails: any;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  selectedService(serviceid: any): void {
    this.router.navigate([`serviceProvider/${this.cardDetails._id}`]);
  }
}
