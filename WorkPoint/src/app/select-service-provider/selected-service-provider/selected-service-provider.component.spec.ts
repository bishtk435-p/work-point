import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectedServiceProviderComponent } from './selected-service-provider.component';

describe('SelectedServiceProviderComponent', () => {
  let component: SelectedServiceProviderComponent;
  let fixture: ComponentFixture<SelectedServiceProviderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectedServiceProviderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectedServiceProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
