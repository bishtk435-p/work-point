import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private socket: Socket,
    private notification: NzNotificationService
  ) { }

  connectNotificationSocket(): void {
    this.socket.on('New Service Request Received', () => {
      this.notification
      .blank(
        'New Service Request',
        'You have received a service request.'
      )
      .onClick.subscribe(() => {
        console.log('notification clicked!');
      });
    });
  }

}
