import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  selectedServiceProviders: any[] = [];
  selectedServiceProviders$ = new BehaviorSubject(this.selectedServiceProviders);
  isslected$ = new BehaviorSubject(false);
  emitFormValue = new Subject();

  constructor() {
    if (localStorage.getItem('cartItems')){
      this.selectedServiceProviders = JSON.parse(localStorage.getItem('cartItems'));
      this.updateSelectedServiceProviders();
    }
  }

  addItemToCart(profileDetails: any): void {
    this.selectedServiceProviders.push(profileDetails);
    localStorage.setItem('cartItems', JSON.stringify(this.selectedServiceProviders));
    this.updateSelectedServiceProviders();
    console.log(this.selectedServiceProviders);
  }

  deleteItemFromCart(id: any): void {
    // this.isslected$.next(false);
    this.selectedServiceProviders = this.selectedServiceProviders.filter((e: any) => {
      return e.serviceProvider._id !== id;
    });
    localStorage.setItem('cartItems', JSON.stringify(this.selectedServiceProviders));
    this.updateSelectedServiceProviders();
  }

  removeAllItems(): void {
    this.selectedServiceProviders = [];
    localStorage.setItem('cartItems', JSON.stringify(this.selectedServiceProviders));
    this.updateSelectedServiceProviders();
  }

  emitFormData(value): any{
    // this.emitFormValue.next(value);
    this.addItemToCart(value);
  }
  updateSelectedServiceProviders(): any {
    this.selectedServiceProviders$.next(this.selectedServiceProviders);
    console.log('service selected==', this.selectedServiceProviders)
  }
}
