import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { env } from 'process';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  updateItem = new EventEmitter();

  constructor(private http: HttpClient) {}

  getAvailableServices(params?: any): Observable<any> {
    return this.http.get(
      `${environment.nodeServerUrl}getAllServices?` + params
    );
  }

  changePassword(payload: any): Observable<any> {
    return this.http.post(
      `${environment.nodeServerUrl}users/auth/changepassword`,
      { payload }
    );
  }

  saveUserRequest(payload: any): Observable<any> {
    return this.http.post(`${environment.nodeServerUrl}users/saverequests`, {
      payload,
    });
  }

  saveFeedback(payload: any): Observable<any> {
    return this.http.post(`${environment.nodeServerUrl}users/submitfeedback`, {
      payload,
    });
  }
  getPaymentDetail(requestId: any) {
    return this.http.get(`${environment.nodeServerUrl}payment/paymentdetails`, {
      params: { requestId },
    });
  }

  savePaymentDetail(payload:any){
    return this.http.post(`${environment.nodeServerUrl}payment/savepaymentdetails`,payload);
  }
}
