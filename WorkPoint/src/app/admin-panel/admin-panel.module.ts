import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminPanelRoutingModule } from './admin-panel-routing.module';
import { AddCategoryComponent } from './add-category/add-category.component';
import { FormsModule } from '@angular/forms';
import { WsCommonModule } from '../common/common.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [AddCategoryComponent, AdminHomeComponent],
  imports: [
    CommonModule,
    AdminPanelRoutingModule,
    FormsModule,
    WsCommonModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
  ],
})
export class AdminPanelModule {}
