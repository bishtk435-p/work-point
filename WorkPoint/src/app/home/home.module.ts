import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectServiceComponent } from './select-service/select-service.component';
import { WsCommonModule } from '../common/common.module';
import { SliderComponent } from './slider/slider.component';
import { FeatureComponent } from './feature/feature.component';
import { FeedbackSliderComponent } from './feedback-slider/feedback-slider.component';
import { HomeComponent } from './home/home.component';
import { HomeRoutingModule } from '../home/home-routing.module';
import { ServicesRoutingModule } from '../select-service-provider/services-routing.modules';
import { ContactusComponent } from './contactus/contactus.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    SliderComponent,
    SelectServiceComponent,
    FeatureComponent,
    FeedbackSliderComponent,
    HomeComponent,
    ContactusComponent,
  ],
  imports: [
    CommonModule,
    WsCommonModule,
    HomeRoutingModule,
    ServicesRoutingModule,
    FormsModule
  ],
  exports: [
    SelectServiceComponent,
    SliderComponent,
    FeatureComponent,
    FeedbackSliderComponent,
  ],
})
export class HomeModule {}
