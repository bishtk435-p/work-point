import { Component, OnInit } from '@angular/core';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { MessageService } from 'src/app/core/Services/message.service';
import { AuthService } from '../auth.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'ws-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.less'],
})
export class SignupComponent implements OnInit {
  accept = true;
  loader = false;
  passwordMatch = null;
  passwordtype = 'password';
  cpasswordtype = 'password';
  passwordrex = new RegExp(
    '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{8,}'
  );

  lowercaseregex = new RegExp('(?=.*[a-z])');
  uppercaseregx = new RegExp('(?=.*[A-Z])');
  digitregex = new RegExp('(?=.*[0-9])');
  specialcharregex = new RegExp('(?=.*[!,@,#,$,%,&])');

  constructor(
    private modal: NzModalRef,
    private authService: AuthService,
    private modalService: NzModalService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {}

  destroyModal(): void {
    this.modal.destroy();
  }
  openLoginModal(): void {
    this.destroyModal();
    this.modalService.create({
      nzTitle: null,
      nzFooter: null,
      nzContent: LoginComponent,
    });
  }

  checkFormData(value: any): boolean {
    if (!this.lowercaseregex.test(value.password)) {
      this.passwordMatch =
        'Password must contain at least one lower case letter';
      return false;
    } else if (!this.uppercaseregx.test(value.password)) {
      this.passwordMatch =
        'Password must contain at least one upper case letter';
      return false;
    } else if (!this.digitregex.test(value.password)) {
      this.passwordMatch = 'Password must contain at least one digit letter';
      return false;
    } else if (!this.specialcharregex.test(value.password)) {
      this.passwordMatch =
        'Password must contain at least one special character';
      return false;
    } else if (value.password.length < 8) {
      this.passwordMatch = 'Password must contain at least content 8 character';
      return false;
    } else if (value.password !== value.cpassword) {
      this.passwordMatch = 'Password Not Matched';
      return false;
    } else if (!this.accept) {
      return false;
    } else {
      return true;
    }
  }

  clearErr(): void {
    this.passwordMatch = null;
  }
  changetype(value): void {
    this.passwordtype = value;
  }
  changetypec(value): void {
    this.cpasswordtype = value;
  }
  getFormData(value): void {
    if (this.checkFormData(value)) {
      this.loader = true;
      this.authService.userSignup(value).subscribe(
        (res) => {
          this.messageService.showMessage(
            'success',
            'Thank you for registation...'
          );
          this.destroyModal();
          this.modalService.create({
            nzTitle: null,
            nzFooter: null,
            nzContent: LoginComponent,
          });
        },
        (err) => {
          this.loader = false;
          if (err.status === 302) {
            this.messageService.showMessage('error', err.error);
          }
        }
      );
    }
  }
}
