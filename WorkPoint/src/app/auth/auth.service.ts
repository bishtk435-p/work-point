import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isuserLoggedIn = new EventEmitter();
  constructor(private http: HttpClient) {}

  isloggedIn(): boolean {
    return !!localStorage.getItem('token');
  }

  getToken(): any {
    return localStorage.getItem('token');
  }
  getCurrentUserName(): any {
    return localStorage
      .getItem('userName')
      .match(/\b(\w)/g)
      .join('')
      .toLocaleUpperCase();
  }

  getCurrentUserRole(): any {
    return localStorage.getItem('userRole');
  }

  userSignup(payload: any): Observable<any> {
    return this.http.post(`${environment.nodeServerUrl}users/signup`, {
      payload,
    });
  }
  userLogin(payload: any): Observable<any> {
    return this.http.post(`${environment.nodeServerUrl}users/auth/login`, {
      payload,
    });
  }
  forgetPassword(payload: any): Observable<any> {
    return this.http.post(`${environment.nodeServerUrl}users/send-mail`, {
      payload,
    });
  }

  resetPassword(payload: any): Observable<any> {
    return this.http.post(
      `${environment.nodeServerUrl}users/auth/resetpassword`,
      { payload }
    );
  }
}
