import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { MessageService } from 'src/app/core/Services/message.service';
import { AuthService } from '../auth.service';
import { SignupComponent } from '../signup/signup.component';

@Component({
  selector: 'ws-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class LoginComponent implements OnInit {
  loader = false;

  passwordtype = 'password';

  constructor(
    private modal: NzModalRef,
    private authService: AuthService,
    private msgService: MessageService,
    private router: Router,
    private modalService: NzModalService
  ) {}

  ngOnInit(): void {}

  changetype(value): void {
    this.passwordtype = value;
  }

  destroyModal(): void {
    this.modal.destroy();
  }

  openRegistrationModal(): void {
    this.destroyModal();
    this.modalService.create({
      nzTitle: null,
      nzFooter: null,
      nzContent: SignupComponent,
    });
  }

  onSubmit(formData: any): void {
    this.loader = true;
    this.authService.userLogin(formData).subscribe(
      (res: any) => {
        this.loader = false;
        if (res.token) {
          this.modal.destroy();
          localStorage.setItem('token', res.token);
          localStorage.setItem('userName', res.userName);
          localStorage.setItem('userid', res.userId);
          localStorage.setItem('userRole', res.userType);
          this.authService.isuserLoggedIn.emit(res);
          this.msgService.showMessage('success', 'Login Successfully');
          if (res.userType === 'admin') {
            this.router.navigate(['/admin/home']);
          } else if (res.userType === 'serviceprovider') {
            this.router.navigate(['/seller/dashboard']);
          }
        }
      },
      (err) => {
        this.loader = false;
        if (err.status === 404) {
          this.msgService.showMessage('error', err.error);
        }
      }
    );
  }

  forgetlinkClick(): void {
    this.destroyModal();
  }
}
