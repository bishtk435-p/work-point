import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { WsCommonModule } from '../common/common.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    SignupComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    WsCommonModule,
    FormsModule,
    RouterModule
  ],
  exports: [
    SignupComponent,
    LoginComponent
  ],
})
export class AuthModule {}
