import { Injectable } from '@angular/core';
import { ActivatedRoute, CanActivate, Router } from '@angular/router';
import { MessageService } from 'src/app/core/Services/message.service';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGaurdGuard implements CanActivate {
  // canActivate(
  //   route: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }
  constructor(
    private authService: AuthService,
    private router: Router,
    private mesgService: MessageService
  ) {}
  canActivate(): boolean {
    if (this.authService.isloggedIn()) {
      // alert('Yessss');
      return true;
    } else {
      this.mesgService.showMessage('error', 'Unauthorized request');
      // this.router.navigate(['']);
      return false;
    }
  }
}
